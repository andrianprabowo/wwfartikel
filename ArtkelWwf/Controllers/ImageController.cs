﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArtkelWwf.Models;

namespace ArtkelWwf.Controllers
{
    public class ImageController : Controller
    {
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(image imageModel)
        {
            string fileName = Path.GetFileNameWithoutExtension(imageModel.ImageFIle.FileName);
            string extension = Path.GetExtension(imageModel.ImageFIle.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            imageModel.ImagePath = "~/Image/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
            imageModel.ImageFIle.SaveAs(fileName);
            using (DbModels db = new DbModels())
            {
                 db.images.Add(imageModel);
                 db.SaveChanges();
                
            }
            ModelState.Clear();
            return View();

        }

        [HttpGet]
        public ActionResult View(int id)
        {
            image imageModel = new image();
            using (DbModels db = new DbModels())
            {
                imageModel = db.images.Where(x => x.ImageID == id).FirstOrDefault();
            }
            return View(imageModel);
        }
    }
}